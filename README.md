# Ben & Jerry's Fan API

**Description**

A scraper for the website [Ben & Jerry](https://www.benjerry.com/), full details are on [Assignment Page](https://gitlab.com/jigzpalillo/benjerry-api/wikis/Assignment).

**Table Structure**

  Pending...

## Testing
Repository will be hosted on [**GitLab**](https://gitlab.com/jigzpalillo/benjerry-api), and [Live Preview](https://benjerry-obscurance-com.herokuapp.com) is made available through Heroku.


## Project Stack

Technologies and Tools used for Project:

|Function                         |Used                       |
|---------------------------------|---------------------------|
|Database                    	    |`MongoDB(MongoDB Atlas)`   |
|Back End                    	    |`Node.js(Heroku)`          |
|Git Repository                   |`GitLab(GitKraken)`        |
|Editor                           |`Visual Code`              |
|Browser                          |`Firefox Developer Edition`|






